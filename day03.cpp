#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

using namespace std;

//Unnecessary and inefficient recursion without pointers
string getMostCommon(vector<string> v, int loc) {
    if(v.size() == 1) {
        return v[0];
    } else {
        vector<string> zero;
        vector<string> one;
        for(string s: v) {
            if(s.at(loc) == '0') {
                zero.push_back(s);
            } else {
                one.push_back(s);
            }
        }
        if(one.size() >= zero.size()) {
            return getMostCommon(one, loc+1);
        } else {
            return getMostCommon(zero, loc+1);
        }
    }
}

string getLeastCommon(vector<string> v, int loc) {
    if(v.size() == 1) {
        return v[0];
    } else {
        vector<string> zero;
        vector<string> one;
        for(string s: v) {
            if(s.at(loc) == '0') {
                zero.push_back(s);
            } else {
                one.push_back(s);
            }
        }
        if(one.size() < zero.size()) {
            return getLeastCommon(one, loc+1);
        } else {
            return getLeastCommon(zero, loc+1);
        }
    }
}

int main() {

    ifstream fin("day03.txt");
    vector<int> v(12, 0);
    vector<string> binaries;
    string s;

    while(!fin.eof()) {
        getline(fin, s);
        binaries.push_back(s);
        for(int i = 0; i < s.size(); i++) {
            if(s.at(i) == '1') {
                v[i]++;
            } else if(s.at(i) == '0') {
                v[i]--;
            }
        }
    }

    int gamma = 0;
    int epsilon = 0;

    for(int i = 0; i < v.size(); i++) {
        if(v[i] > 0) {
            gamma += (int)pow(2.0, v.size()-(i+1));
        } else {
            epsilon += (int)pow(2.0, v.size()-(i+1));
        }
    }

    cout << gamma * epsilon << endl;

    int o2 = stoi(getMostCommon(binaries, 0), nullptr, 2);
    int co2 = stoi(getLeastCommon(binaries, 0), nullptr, 2);

    cout << o2 * co2 << endl;

}
