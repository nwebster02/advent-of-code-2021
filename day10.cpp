#include <iostream>
#include <fstream>
#include <stack>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

    ifstream fin("day10.txt");
    int points = 0;
    vector<long long> v;

    while(!fin.eof()) {
        stack<char> stack;
        string s;
        getline(fin, s);
        bool test = true;
        for(char c: s) {
            if(c == '(' || c == '[' || c == '{' || c == '<') {
                stack.push(c);
            } else if (test) {
                if(c == ')') {
                    if (stack.top() != '(') {
                        points += 3;
                        test = false;
                    }
                } else if(c == ']') {
                    if(stack.top() != '[') {
                        points += 57;
                        test = false;
                    }
                } else if(c == '}') {
                    if(stack.top() != '{') {
                        points += 1197;
                        test = false;
                    }
                } else if(c == '>') {
                    if(stack.top() != '<') {
                        points += 25137;
                        test = false;
                    }
                }
                stack.pop();
            }
        }
        long long score = 0;
        while(!stack.empty() && test) {
            score *= 5;
            if(stack.top() == '(') {
                score += 1;
            } else if(stack.top() == '[') {
                score += 2;
            } else if(stack.top() == '{') {
                score += 3;
            } else if(stack.top() == '<') {
                score += 4;
            }
            stack.pop();
        }
        if(score != 0)
            v.push_back(score);
    }

    cout << points << endl;
    sort(v.begin(), v.end());
    cout << v[v.size()/2];

}
