#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main() {

    ifstream fin("day01.txt");
    string s;
    vector<int> v;
    int count = 0;

    while(!fin.eof()) {
        getline(fin, s);
        int num = stoi(s);
        v.push_back(num);
    }

    for(int i = 1; i < v.size(); i++) {
        if(v[i] > v[i-1])
            count++;
    }

    cout << count << endl;
    count = 0;

    for(int i = 3; i < v.size(); i++) {
        if(v[i-2] + v[i-1] + v[i] > v[i-3] + v[i-2] + v[i-1])
            count++;
    }

    cout << count << endl;

}
