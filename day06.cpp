#include <iostream>
#include <fstream>
#include <list>
#include <vector>

using namespace std;

class lanternfish {
public:
    int age;
    lanternfish() { age = 0; }
    lanternfish(int a) { age = a; };
    lanternfish timer(){
        if(age == 0) {
            age = 6;
            return lanternfish(8);
        }
        age--;
        return lanternfish();
    }
};

int main() {

    //Part 1
    ifstream fin("day06.txt");
    string line;

    getline(fin, line);
    list<lanternfish> l;

    for(int i = 0; i < line.size(); i++) {
        if(i % 2 == 0) {
            l.emplace_back(lanternfish((int)line.at(i)-(int)'0'));
        }
    }

    for(int i = 0; i < 80; i++) {
        auto it = l.begin();
        while(it != l.end()) {
            lanternfish fish = (*it).timer();
            if (fish.age != 0) {
                l.emplace_front(fish);
            }
            it++;
        }
    }

    cout << l.size() << endl;
    fin.close();

    //Part 2 (and functional for part 1)
    fin.open("day06.txt");
    getline(fin, line);
    vector<long long> v(9);
    vector<long long> new_v(9);

    for(int i = 0; i < line.size(); i++) {
        if(i % 2 == 0) {
            v[(int)line.at(i)-(int)'0']++;
        }
    }

    for(int i = 0; i < 256; i++) {
        new_v[6] += v[0];
        new_v[8] += v[0];
        for(int j = 1; j < v.size(); j++) {
            new_v[j-1] += v[j];
        }
        for(int j = 0; j < v.size(); j++) {
            v[j] = new_v[j];
            new_v[j] = 0;
        }
    }

    long long sum = 0;
    for(int i = 0; i <= 8; i++) {
        sum += v[i];
    }

    cout << sum << endl;

}
