#include <iostream>
#include <fstream>
#include <map>
#include <set>

using namespace std;

int find_path(map<string, set<string>> &connections, set<string> &visited, string current, bool twice) {
    if(current == "end") {
        return 1;
    } else {
        int count = 0;
        for(string cave: connections[current]) {
            if(cave.at(0) <= 'Z') {
                count += find_path(connections, visited, cave, twice);
            } else if(visited.count(cave) == 0) {
                visited.emplace(cave);
                count += find_path(connections, visited, cave, twice);
                visited.erase(cave);
            } else if(twice && cave != "start") {
                twice = false;
                count += find_path(connections, visited, cave, twice);
                twice = true;
            }
        }
        return count;
    }
}

int main() {

    ifstream fin("day12.txt");
    map<string, set<string>> connections;

    while(!fin.eof()) {
        string s;
        getline(fin, s);
        string cave1 = s.substr(0, s.find('-'));
        string cave2 = s.substr(s.find('-')+1);
        connections[cave1].emplace(cave2);
        connections[cave2].emplace(cave1);
    }

    set<string> visited {"start"};
    cout << find_path(connections, visited, "start", false) << endl;
    cout << find_path(connections, visited, "start", true) << endl;

}
