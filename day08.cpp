#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <set>

using namespace std;

bool equals(string s1, string s2) {
    if(s1.size() != s2.size())
        return false;
    bool test = true;
    for(char c1: s1) {
        bool con = false;
        for(char c2: s2) {
            if(c1 == c2) {
                con = true;
            }
        }
        test &= con;
    }
    return test;
}

bool contains(string s, char c) {
    return s.find(c) < s.size();
}

int total(char c, set<string> &nums) {
    int count = 0;
    for(string s: nums) {
        if(contains(s, c)) {
            count++;
        }
    }
    return count;
}

int determine_number(string s, map<char,char> &segs) {
    if(s.size() == 5) {
        if(!contains(s, segs['f'])) {
            return 2;
        } else if(contains(s, segs['c'])) {
            return 3;
        } else {
            return 5;
        }
    } else if(s.size() == 6) {
        if(!contains(s, segs['d'])) {
            return 0;
        } else if(contains(s, segs['e'])) {
            return 6;
        } else {
            return 9;
        }
    }
}

int main() {

    ifstream fin("day08.txt");
    string str;
    int count1 = 0;
    int count2 = 0;
    while(!fin.eof()) {
        getline(fin, str);
        string val;
        vector<string> patterns;
        vector<string> output;
        bool test = false;
        for(int i = 0; i < str.size(); i++) {
            if(str.at(i) >= 'a' && str.at(i) <= 'z') {
                val.push_back(str.at(i));
            } else if (str.at(i) == ' ' && !val.empty()) {
                if(test) {
                    output.push_back(val);
                    val = "";
                } else {
                    patterns.push_back(val);
                    val = "";
                }
            } else if (str.at(i) == '|') {
                test = true;
            }
        }
        output.push_back(val);
        map<int, string> known;
        set<string> nums;
        map<char, char> segments;
        for(string s: output) {
            if(s.size() >= 2 && s.size() <= 4 || s.size() == 7) {
                count1 ++;
            }
        }
        for(string s: patterns) {
            if(s.size() == 2) {
                known[1] = s;
            } else if (s.size() == 3) {
                known[7] = s;
            } else if (s.size() == 4) {
                known[4] = s;
            } else if (s.size() == 7) {
                known[8] = s;
            }
            nums.emplace(s);
        }
        for(char c = 'a'; c <= 'g'; c++) {
            if(total(c, nums) == 4) {
                segments['e'] = c;
            } else if(total(c, nums) == 6) {
                segments['b'] = c;
            } else if(total(c, nums) == 7) {
                if(contains(known[4], c)) {
                    segments['d'] = c;
                } else {
                    segments['g'] = c;
                }
            } else if(total(c, nums) == 8) {
                if(contains(known[1], c)) {
                    segments['c'] = c;
                } else {
                    segments['a'] = c;
                }
            } else if(total(c, nums) == 9) {
                segments['f'] = c;
            }
        }
        for(string s: nums) {
            if(s.size() == 5 || s.size() == 6) {
                known[determine_number(s, segments)] = s;
            }
        }
        val = "";
        for(string s: output) {
            for(auto p: known) {
                if(equals(s, p.second)) {
                    val += to_string(p.first);
                }
            }
        }
        count2 += stoi(val);
    }

    cout << count1 << endl;
    cout << count2 << endl;

}
