#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

bool check_around(int (&arr)[100][100], int r, int c) {
    bool test = true;
    if(r > 0)
        test &= arr[r][c] < arr[r-1][c];
    if(r < 99)
        test &= arr[r][c] < arr[r+1][c];
    if(c > 0)
        test &= arr[r][c] < arr[r][c-1];
    if(c < 99)
        test &= arr[r][c] < arr[r][c+1];
    return test;
}

int basin_size(int (&arr)[100][100], int r, int c) {
    if(r >= 0 && r < 100 && c >= 0 && c < 100 && arr[r][c] != 9) {
        arr[r][c] = 9;
        int size = 1;
        size += basin_size(arr, r-1, c);
        size += basin_size(arr, r+1, c);
        size += basin_size(arr, r, c-1);
        size += basin_size(arr, r, c+1);
        return size;
    }
    return 0;
}

int main() {

    ifstream fin("day09.txt");
    int surface[100][100];

    for(int r = 0; r < 100; r++) {
        string s;
        getline(fin, s);
        for(int c = 0; c < 100; c++) {
            int num = s.at(c)-'0';
            surface[r][c] = num;
        }
    }

    int sum = 0;
    for(int r = 0; r < 100; r++) {
        for(int c = 0; c < 100; c++) {
            if(check_around(surface, r, c)) {
                sum += 1 + surface[r][c];
            }
        }
    }

    cout << sum << endl;

    vector<int> v;
    for(int r = 0; r < 100; r++) {
        for(int c = 0; c < 100; c++) {
            v.push_back(basin_size(surface, r, c));
        }
    }

    sort(v.begin(), v.end());
    cout << v[v.size()-1] * v[v.size()-2] * v[v.size()-3] << endl;

}
