#include <iostream>
#include <fstream>
#include <map>

using namespace std;

int main() {

    ifstream fin("day14.txt");
    string in;
    map<string, string> temp;

    getline(fin, in);
    string polymer = in;
    while(!fin.eof()) {
        string s;
        getline(fin, s);
        if(!s.empty()) {
            temp.emplace(s.substr(0, 2), s.substr(6));
        }
    }

    //Part 1: Not Scalable
    for(int i = 0; i < 10; i++) {
        string new_polymer;
        for(int j = 0; j < polymer.size()-1; j++) {
            new_polymer += polymer.at(j);
            new_polymer += temp[polymer.substr(j, 2)];
        }
        new_polymer += polymer.at(polymer.size()-1);
        polymer = new_polymer;
    }

    map<char, long long> elements;
    for(char c: polymer) {
        elements[c]++;
    }

    long long minimum = LONG_LONG_MAX;
    long long maximum = LONG_LONG_MIN;

    for(auto p: elements) {
        maximum = max(maximum, p.second);
        minimum = min(minimum, p.second);
    }

    cout << maximum - minimum << endl;
    elements.clear();
    minimum = LONG_LONG_MAX;
    maximum = LONG_LONG_MIN;

    //More Scalable Solution
    map<string, long long> pairs;
    for(int i = 0; i < in.size()-1; i++) {
        pairs[in.substr(i, 2)]++;
    }

    for(int i = 0; i < 40; i++) {
        map<string, long long> new_pairs;
        for (auto p: pairs) {
            string key1 = p.first.substr(0, 1) + temp[p.first];
            string key2 = temp[p.first] + p.first.substr(1);
            new_pairs[key1] += p.second;
            new_pairs[key2] += p.second;
        }
        pairs = new_pairs;
    }

    for(auto p: pairs) {
        elements[p.first.at(0)] += p.second;
        elements[p.first.at(1)] += p.second;
    }

    for(auto p: elements) {
        p.second /= 2;
        if(p.first == in.at(0) || p.first == in.at(in.size()-1)) {
            p.second++;
        }
        maximum = max(maximum, p.second);
        minimum = min(minimum, p.second);
    }

    cout << maximum - minimum << endl;

}
