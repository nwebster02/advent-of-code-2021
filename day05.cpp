#include <iostream>
#include <fstream>
#include <vector>
#include <map>

using namespace std;

int main() {

    ifstream fin("day05.txt");
    string str;
    map<vector<int>, int> graph;

    while(!fin.eof()) {
        vector<int> v;
        getline(fin, str);
        string num;
        for(int i = 0; i < str.size(); i++) {
            if(str.at(i) >= '0' && str.at(i) <= '9') {
                num += str.at(i);
            } else if(!num.empty()){
                v.push_back(stoi(num));
                num = "";
            }
        }
        if(!num.empty())
            v.push_back(stoi(num));
        int minx = min(v[0],v[2]);
        int maxx = max(v[0],v[2]);
        int miny = min(v[1],v[3]);
        int maxy = max(v[1],v[3]);
        if(minx == maxx) {
            for(int i = miny; i <= maxy; i++) {
                vector<int> p = {minx, i};
                graph[p]++;
            }
        } else if (miny == maxy) {
            for(int i = minx; i <= maxx; i++) {
                vector<int> p = {i, miny};
                graph[p]++;
            }
        } else if ((v[0] < v[2] && v[1] < v[3]) || (v[2] < v[0] && v[3] < v[1])){
            for(int i = minx, j = miny; i <= maxx; i++, j++) {
                vector<int> p = {i, j};
                graph[p]++;
            }
        } else {
            for(int i = minx, j = maxy; i <= maxx; i++, j--) {
                vector<int> p = {i, j};
                graph[p]++;
            }
        }
    }

    int count = 0;
    for(auto p: graph) {
        if(p.second > 1) {
            count++;
        }
    }

    cout << count << endl;

}
