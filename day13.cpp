#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <array>

using namespace std;

class Point {
public:
    Point(int x, int y) { this->x = x; this-> y = y; }

    void fold_x(int n) {
        if(x > n) {
            x = n - (x-n);
        }
    }

    void fold_y(int n) {
        if(y > n) {
            y = n - (y-n);
        }
    }

    vector<int> to_vector() {
        return vector<int> {x, y};
    }

    int x;
    int y;
};

int main() {

    ifstream fin("day13.txt");
    vector<Point*> v;
    bool test = true;
    int last_x = 0;
    int last_y = 0;

    while(!fin.eof()) {
        string s;
        getline(fin, s);
        if(test) {
            if(s.empty()) {
                test = false;
            } else {
                int x = stoi(s.substr(0, s.find(',')));
                int y = stoi(s.substr(s.find(',')+1));
                v.emplace_back(new Point(x, y));
            }
        } else {
            int n = stoi(s.substr(s.find('=')+1));
            if(s.find('x') < s.length()) {
                last_x = n;
                for(Point *p: v) {
                    p->fold_x(n);
                }
            } else {
                last_y = n;
                for(Point *p: v) {
                    p->fold_y(n);
                }
            }
            set<vector<int>> points;
            for(Point *p: v) {
                points.emplace(p->to_vector());
            }

            cout << points.size() << endl;

            if(fin.eof()) {
                cout << endl;
                int display[last_x][last_y];
                for(vector<int> p: points) {
                    display[p[0]][p[1]] = 1;
                }
                for(int c = 0; c < last_y; c++) {
                    string output;
                    for(int r = 0; r < last_x; r++) {
                        if(display[r][c] == 1) {
                            output += "#";
                        } else {
                            output += " ";
                        }
                    }
                    cout << output << endl;
                }
            }
        }
    }
}
