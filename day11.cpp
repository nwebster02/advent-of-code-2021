#include <iostream>
#include <fstream>

using namespace std;

int blink_count = 0;

void light_up(int (&graph)[10][10], int r, int c, bool test) {
    if(r >= 0 && r < 10 && c >= 0 && c < 10 && graph[r][c] != 0) {
        graph[r][c]++;
    }
    if(test) {
        light_up(graph, r-1, c, false);
        light_up(graph, r+1, c, false);
        light_up(graph, r, c-1, false);
        light_up(graph, r, c+1, false);
        light_up(graph, r-1, c-1, false);
        light_up(graph, r-1, c+1, false);
        light_up(graph, r+1, c-1, false);
        light_up(graph, r+1, c+1, false);
    }
}

int main() {

    ifstream fin("day11.txt");
    string s;
    int graph[10][10];

    for(int r = 0; r < 10; r++) {
        getline(fin, s);
        for(int c = 0; c < 10; c++) {
            graph[r][c] = s.at(c)-'0';
        }
    }

    for(int i = 0; i < 500; i++) {
        for(int r = 0; r < 10; r++)
            for(int c = 0; c < 10; c++)
                graph[r][c]++;
        bool test = true;
        while(test) {
            test = false;
            for(int r = 0; r < 10; r++) {
                for(int c = 0; c < 10; c++) {
                    if(graph[r][c] > 9) {
                        blink_count++;
                        graph[r][c] = 0;
                        light_up(graph, r, c, true);
                        test = true;
                    }
                }
            }
        }
        if(i == 99) {
            cout << blink_count << endl;
        }
        bool blink = true;
        for(int r = 0; r < 10; r++)
            for(int c = 0; c < 10; c++)
                if(graph[r][c] != 0)
                    blink = false;
        if(blink) {
            cout << i+1 << endl;
            break;
        }
    }


}
