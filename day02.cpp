#include <iostream>
#include <fstream>

using namespace std;

int main() {

    ifstream fin("day02.txt");
    string s;
    int h = 0;
    int v = 0;
    int a = 0;

    while(!fin.eof()) {
        getline(fin, s);
        string type = s.substr(0, s.find(' '));
        int val = stoi(s.substr(s.find(' ')+1));
        if(type == "forward") {
            h += val;
            v += a * val;
        } else if(type == "down") {
            a += val;
        } else if (type == "up") {
            a -= val;
        }
    }

    cout << h * v << endl;

}
