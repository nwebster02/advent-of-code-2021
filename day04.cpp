#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class bingo_card{
public:
    int card[5][5];
    bool done = false;
    bingo_card(vector<int> v){
        for(int r = 0; r < 5; r++) {
            for(int c = 0; c < 5; c++) {
                card[r][c] = v[(r*5)+c];
            }
        }
    }
    bool mark(int n) {
        for(int r = 0; r < 5; r++) {
            for(int c = 0; c < 5; c++) {
                if(card[r][c] == n) {
                    card[r][c] = -1;
                    return true;
                }
            }
        }
        return false;
    }
    bool bingo() {
        if(done)
            return false;
        for(int r = 0; r < 5; r++) {
            if(card[r][0] + card[r][1] + card[r][2] + card[r][3] + card[r][4] == -5) {
                done = true;
                return true;
            }
        }
        for(int c = 0; c < 5; c++) {
            if(card[0][c] + card[1][c] + card[2][c] + card[3][c] + card[4][c] == -5) {
                done = true;
                return true;
            }
        }
        return false;
    }
    int sum() {
        int n = 0;
        for(int r = 0; r < 5; r++) {
            for(int c = 0; c < 5; c++) {
                if(card[r][c] != -1) {
                    n += card[r][c];
                }
            }
        }
        return n;
    }
};

int main() {

    ifstream fin("day04.txt");
    string line;
    vector<int> v;
    vector<bingo_card> cards;
    getline(fin, line);

    string num;
    for(int i = 0; i < line.size(); i++) {
        if(line.at(i) == ',') {
            v.push_back(stoi(num));
            num = "";
        } else {
            num += line.at(i);
        }
    }
    v.push_back(stoi(num));

    while(!fin.eof()) {
        vector<int> card;
        for(int i = 0; i < 25; i++) {
            int n;
            fin >> n;
            card.push_back(n);
        }
        bingo_card c(card);
        cards.push_back(c);
    }

    for(int i = 0; i < v.size(); i++) {
        for(int j = 0; j < cards.size(); j++) {
            if(cards[j].mark(v[i])) {
                if(cards[j].bingo()) {
                    cout << cards[j].sum() * v[i] << endl;
                }
            }
        }
    }

}
