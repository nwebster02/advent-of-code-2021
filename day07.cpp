#include <iostream>
#include <fstream>
#include <vector>
#include <map>

using namespace std;

int main() {

    ifstream fin("day07.txt");
    string str;
    map<int, int> loc;
    getline(fin, str);
    string num;
    for(int i = 0; i < str.size(); i++) {
        if(str.at(i) >= '0' && str.at(i) <= '9') {
            num += str.at(i);
        } else {
            loc[stoi(num)]++;
            num = "";
        }
    }
    loc[stoi(num)]++;

    int lowest_cost = INT_MAX;
    int max = (--loc.end())->first;
    for(int i = 0; i <= max; i++) {
        int cost = 0;
        for(auto p: loc) {
            cost += abs(p.first-i) * p.second;
        }
        lowest_cost = min(cost, lowest_cost);
    }

    cout << lowest_cost << endl;

    lowest_cost = INT_MAX;
    for(int i = 0; i <= max; i++) {
        int cost = 0;
        for(auto p: loc) {
            cost += (abs(p.first-i) * (abs(p.first-i)+1) / 2) * p.second;
        }
        lowest_cost = min(cost, lowest_cost);
    }

    cout << lowest_cost << endl;

}
